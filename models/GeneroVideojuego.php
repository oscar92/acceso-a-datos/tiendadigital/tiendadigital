<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "genero_videojuego".
 *
 * @property int|null $codigo_genero
 * @property int|null $codigo_videojuego
 * @property int $codigo_genero_videojuego
 *
 * @property Genero $codigoGenero
 * @property Videojuego $codigoVideojuego
 */
class GeneroVideojuego extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'genero_videojuego';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_genero', 'codigo_videojuego'], 'integer'],
            [['codigo_genero'], 'exist', 'skipOnError' => true, 'targetClass' => Genero::className(), 'targetAttribute' => ['codigo_genero' => 'codigo_genero']],
            [['codigo_videojuego'], 'exist', 'skipOnError' => true, 'targetClass' => Videojuego::className(), 'targetAttribute' => ['codigo_videojuego' => 'codigo_videojuego']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_genero' => 'Codigo Genero',
            'codigo_videojuego' => 'Codigo Videojuego',
            'codigo_genero_videojuego' => 'Codigo Genero Videojuego',
        ];
    }

    /**
     * Gets query for [[CodigoGenero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoGenero()
    {
        return $this->hasOne(Genero::className(), ['codigo_genero' => 'codigo_genero']);
    }

    /**
     * Gets query for [[CodigoVideojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoVideojuego()
    {
        return $this->hasOne(Videojuego::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }
}
