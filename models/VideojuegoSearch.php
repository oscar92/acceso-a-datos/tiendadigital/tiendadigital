<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Videojuego;

/**
 * VideojuegoSearch represents the model behind the search form of `app\models\Videojuego`.
 */
class VideojuegoSearch extends Videojuego
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_videojuego', 'codigo_desarrollador', 'codigo_distribuidor'], 'integer'],
            [['nombre', 'fecha_lanzamiento', 'descripcion', 'SO', 'procesador', 'memoria', 'grafica', 'almacenamiento'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Videojuego::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_videojuego' => $this->codigo_videojuego,
            'codigo_desarrollador' => $this->codigo_desarrollador,
            'codigo_distribuidor' => $this->codigo_distribuidor,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'fecha_lanzamiento', $this->fecha_lanzamiento])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'SO', $this->SO])
            ->andFilterWhere(['like', 'procesador', $this->procesador])
            ->andFilterWhere(['like', 'memoria', $this->memoria])
            ->andFilterWhere(['like', 'grafica', $this->grafica])
            ->andFilterWhere(['like', 'almacenamiento', $this->almacenamiento]);

        return $dataProvider;
    }
}
