<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "videojuego".
 *
 * @property int $codigo_videojuego
 * @property int|null $codigo_desarrollador
 * @property int|null $codigo_distribuidor
 * @property string|null $nombre
 * @property string|null $fecha_lanzamiento
 * @property string|null $descripcion
 * @property string|null $SO
 * @property string|null $procesador
 * @property string|null $memoria
 * @property string|null $grafica
 * @property string|null $almacenamiento
 *
 * @property AnalistaVideojuego[] $analistaVideojuegos
 * @property BibliotecaVideojuego[] $bibliotecaVideojuegos
 * @property Comentarios[] $comentarios
 * @property Dlc[] $dlcs
 * @property GeneroVideojuego[] $generoVideojuegos
 * @property UsuarioVideojuego[] $usuarioVideojuegos
 * @property Version[] $versions
 * @property Desarrollador $codigoDesarrollador
 * @property Distribuidor $codigoDistribuidor
 * @property VideojuegoEtiqueta[] $videojuegoEtiquetas
 */
class Videojuego extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'videojuego';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_desarrollador', 'codigo_distribuidor'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['fecha_lanzamiento'], 'string', 'max' => 10],
            [['descripcion'], 'string', 'max' => 5000],
            [['SO', 'procesador', 'memoria', 'grafica', 'almacenamiento'], 'string', 'max' => 20],
            [['nombre'], 'unique'],
            [['codigo_desarrollador'], 'exist', 'skipOnError' => true, 'targetClass' => Desarrollador::className(), 'targetAttribute' => ['codigo_desarrollador' => 'codigo_desarrollador']],
            [['codigo_distribuidor'], 'exist', 'skipOnError' => true, 'targetClass' => Distribuidor::className(), 'targetAttribute' => ['codigo_distribuidor' => 'codigo_distribuidor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_videojuego' => 'Codigo Videojuego',
            'codigo_desarrollador' => 'Codigo Desarrollador',
            'codigo_distribuidor' => 'Codigo Distribuidor',
            'nombre' => 'Nombre',
            'fecha_lanzamiento' => 'Fecha Lanzamiento',
            'descripcion' => 'Descripcion',
            'SO' => 'So',
            'procesador' => 'Procesador',
            'memoria' => 'Memoria',
            'grafica' => 'Grafica',
            'almacenamiento' => 'Almacenamiento',
        ];
    }

    /**
     * Gets query for [[AnalistaVideojuegos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnalistaVideojuegos()
    {
        return $this->hasMany(AnalistaVideojuego::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }

    /**
     * Gets query for [[BibliotecaVideojuegos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBibliotecaVideojuegos()
    {
        return $this->hasMany(BibliotecaVideojuego::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasMany(Comentarios::className(), ['juego' => 'nombre']);
    }

    /**
     * Gets query for [[Dlcs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDlcs()
    {
        return $this->hasMany(Dlc::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }

    /**
     * Gets query for [[GeneroVideojuegos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGeneroVideojuegos()
    {
        return $this->hasMany(GeneroVideojuego::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }

    /**
     * Gets query for [[UsuarioVideojuegos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioVideojuegos()
    {
        return $this->hasMany(UsuarioVideojuego::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }

    /**
     * Gets query for [[Versions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVersions()
    {
        return $this->hasMany(Version::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }

    /**
     * Gets query for [[CodigoDesarrollador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDesarrollador()
    {
        return $this->hasOne(Desarrollador::className(), ['codigo_desarrollador' => 'codigo_desarrollador']);
    }

    /**
     * Gets query for [[CodigoDistribuidor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDistribuidor()
    {
        return $this->hasOne(Distribuidor::className(), ['codigo_distribuidor' => 'codigo_distribuidor']);
    }

    /**
     * Gets query for [[VideojuegoEtiquetas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVideojuegoEtiquetas()
    {
        return $this->hasMany(VideojuegoEtiqueta::className(), ['videojuego' => 'nombre']);
    }
}
