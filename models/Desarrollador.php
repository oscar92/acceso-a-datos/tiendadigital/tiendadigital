<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "desarrollador".
 *
 * @property int $codigo_desarrollador
 * @property string|null $nombre
 *
 * @property Videojuego $videojuego
 */
class Desarrollador extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'desarrollador';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_desarrollador'], 'required'],
            [['codigo_desarrollador'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['codigo_desarrollador'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_desarrollador' => 'Codigo Desarrollador',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Videojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVideojuego()
    {
        return $this->hasOne(Videojuego::className(), ['codigo_desarrollador' => 'codigo_desarrollador']);
    }
}
