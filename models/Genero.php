<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "genero".
 *
 * @property string|null $genero
 * @property int $codigo_genero
 *
 * @property GeneroVideojuego[] $generoVideojuegos
 */
class Genero extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'genero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['genero'], 'string', 'max' => 30],
            [['genero'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'genero' => 'Genero',
            'codigo_genero' => 'Codigo Genero',
        ];
    }

    /**
     * Gets query for [[GeneroVideojuegos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGeneroVideojuegos()
    {
        return $this->hasMany(GeneroVideojuego::className(), ['codigo_genero' => 'codigo_genero']);
    }
}
