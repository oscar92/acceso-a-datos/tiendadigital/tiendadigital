<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "analista".
 *
 * @property int $codigo_analista
 * @property string|null $nombre
 *
 * @property AnalistaVideojuego $analistaVideojuego
 */
class Analista extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'analista';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_analista'], 'required'],
            [['codigo_analista'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['codigo_analista'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_analista' => 'Codigo Analista',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[AnalistaVideojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnalistaVideojuego()
    {
        return $this->hasOne(AnalistaVideojuego::className(), ['codigo_analista' => 'codigo_analista']);
    }
}
