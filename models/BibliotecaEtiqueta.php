<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "biblioteca_etiqueta".
 *
 * @property int|null $codigo_biblioteca
 * @property int|null $codigo_etiqueta
 * @property int $codigo_biblioteca_etiqueta
 *
 * @property Biblioteca $codigoBiblioteca
 * @property Etiquetas $codigoEtiqueta
 */
class BibliotecaEtiqueta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'biblioteca_etiqueta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_biblioteca', 'codigo_etiqueta'], 'integer'],
            [['codigo_biblioteca'], 'exist', 'skipOnError' => true, 'targetClass' => Biblioteca::className(), 'targetAttribute' => ['codigo_biblioteca' => 'codigo_biblioteca']],
            [['codigo_etiqueta'], 'exist', 'skipOnError' => true, 'targetClass' => Etiquetas::className(), 'targetAttribute' => ['codigo_etiqueta' => 'codigo_etiqueta']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_biblioteca' => 'Codigo Biblioteca',
            'codigo_etiqueta' => 'Codigo Etiqueta',
            'codigo_biblioteca_etiqueta' => 'Codigo Biblioteca Etiqueta',
        ];
    }

    /**
     * Gets query for [[CodigoBiblioteca]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoBiblioteca()
    {
        return $this->hasOne(Biblioteca::className(), ['codigo_biblioteca' => 'codigo_biblioteca']);
    }

    /**
     * Gets query for [[CodigoEtiqueta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEtiqueta()
    {
        return $this->hasOne(Etiquetas::className(), ['codigo_etiqueta' => 'codigo_etiqueta']);
    }
}
