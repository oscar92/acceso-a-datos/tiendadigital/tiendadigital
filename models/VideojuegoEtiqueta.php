<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "videojuego_etiqueta".
 *
 * @property string|null $videojuego
 * @property string|null $etiqueta
 * @property int $codigo_videojuego_etiqueta
 *
 * @property Videojuego $videojuego0
 * @property Etiquetas $etiqueta0
 */
class VideojuegoEtiqueta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'videojuego_etiqueta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['videojuego', 'etiqueta'], 'string', 'max' => 30],
            [['videojuego'], 'exist', 'skipOnError' => true, 'targetClass' => Videojuego::className(), 'targetAttribute' => ['videojuego' => 'nombre']],
            [['etiqueta'], 'exist', 'skipOnError' => true, 'targetClass' => Etiquetas::className(), 'targetAttribute' => ['etiqueta' => 'etiqueta']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'videojuego' => 'Videojuego',
            'etiqueta' => 'Etiqueta',
            'codigo_videojuego_etiqueta' => 'Codigo Videojuego Etiqueta',
        ];
    }

    /**
     * Gets query for [[Videojuego0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVideojuego0()
    {
        return $this->hasOne(Videojuego::className(), ['nombre' => 'videojuego']);
    }

    /**
     * Gets query for [[Etiqueta0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEtiqueta0()
    {
        return $this->hasOne(Etiquetas::className(), ['etiqueta' => 'etiqueta']);
    }
}
