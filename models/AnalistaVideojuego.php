<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "analista_videojuego".
 *
 * @property int|null $codigo_analista
 * @property int|null $codigo_videojuego
 * @property string|null $puntuacion
 * @property string|null $analisis
 * @property int $codigo_analista_videojuego
 *
 * @property Analista $codigoAnalista
 * @property Videojuego $codigoVideojuego
 */
class AnalistaVideojuego extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'analista_videojuego';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_analista', 'codigo_videojuego', 'codigo_analista_videojuego'], 'integer'],
            [['codigo_analista_videojuego'], 'required'],
            [['puntuacion'], 'string', 'max' => 5],
            [['analisis'], 'string', 'max' => 50],
            [['codigo_analista'], 'unique'],
            [['codigo_videojuego'], 'unique'],
            [['codigo_analista_videojuego'], 'unique'],
            [['codigo_analista'], 'exist', 'skipOnError' => true, 'targetClass' => Analista::className(), 'targetAttribute' => ['codigo_analista' => 'codigo_analista']],
            [['codigo_videojuego'], 'exist', 'skipOnError' => true, 'targetClass' => Videojuego::className(), 'targetAttribute' => ['codigo_videojuego' => 'codigo_videojuego']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_analista' => 'Codigo Analista',
            'codigo_videojuego' => 'Codigo Videojuego',
            'puntuacion' => 'Puntuacion',
            'analisis' => 'Analisis',
            'codigo_analista_videojuego' => 'Codigo Analista Videojuego',
        ];
    }

    /**
     * Gets query for [[CodigoAnalista]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAnalista()
    {
        return $this->hasOne(Analista::className(), ['codigo_analista' => 'codigo_analista']);
    }

    /**
     * Gets query for [[CodigoVideojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoVideojuego()
    {
        return $this->hasOne(Videojuego::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }
}
