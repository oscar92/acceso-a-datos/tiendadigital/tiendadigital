<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comentarios".
 *
 * @property int $codigo_comentario
 * @property string|null $usuario
 * @property string|null $juego
 * @property string|null $comentario
 * @property int|null $valoracion
 *
 * @property Videojuego $juego0
 * @property Usuario $usuario0
 */
class Comentarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comentarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_comentario'], 'required'],
            [['codigo_comentario', 'valoracion'], 'integer'],
            [['usuario', 'juego'], 'string', 'max' => 30],
            [['comentario'], 'string', 'max' => 5000],
            [['codigo_comentario'], 'unique'],
            [['juego'], 'exist', 'skipOnError' => true, 'targetClass' => Videojuego::className(), 'targetAttribute' => ['juego' => 'nombre']],
            [['usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_comentario' => 'Codigo Comentario',
            'usuario' => 'Usuario',
            'juego' => 'Juego',
            'comentario' => 'Comentario',
            'valoracion' => 'Valoracion',
        ];
    }

    /**
     * Gets query for [[Juego0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuego0()
    {
        return $this->hasOne(Videojuego::className(), ['nombre' => 'juego']);
    }

    /**
     * Gets query for [[Usuario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['nombre' => 'usuario']);
    }
}
