<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "etiquetas".
 *
 * @property string|null $etiqueta
 * @property int $codigo_etiqueta
 *
 * @property BibliotecaEtiqueta[] $bibliotecaEtiquetas
 * @property VideojuegoEtiqueta[] $videojuegoEtiquetas
 * @property VideojuegoEtiqueta[] $videojuegoEtiquetas0
 */
class Etiquetas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'etiquetas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['etiqueta'], 'string', 'max' => 30],
            [['etiqueta'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'etiqueta' => 'Etiqueta',
            'codigo_etiqueta' => 'Codigo Etiqueta',
        ];
    }

    /**
     * Gets query for [[BibliotecaEtiquetas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBibliotecaEtiquetas()
    {
        return $this->hasMany(BibliotecaEtiqueta::className(), ['codigo_etiqueta' => 'codigo_etiqueta']);
    }

    /**
     * Gets query for [[VideojuegoEtiquetas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVideojuegoEtiquetas()
    {
        return $this->hasMany(VideojuegoEtiqueta::className(), ['codigo_etiqueta' => 'codigo_etiqueta']);
    }

    /**
     * Gets query for [[VideojuegoEtiquetas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVideojuegoEtiquetas0()
    {
        return $this->hasMany(VideojuegoEtiqueta::className(), ['etiqueta' => 'etiqueta']);
    }
}
