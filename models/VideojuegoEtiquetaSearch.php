<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VideojuegoEtiqueta;

/**
 * VideojuegoEtiquetaSearch represents the model behind the search form of `app\models\VideojuegoEtiqueta`.
 */
class VideojuegoEtiquetaSearch extends VideojuegoEtiqueta
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['videojuego', 'etiqueta'], 'safe'],
            [['codigo_videojuego_etiqueta'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VideojuegoEtiqueta::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo_videojuego_etiqueta' => $this->codigo_videojuego_etiqueta,
        ]);

        $query->andFilterWhere(['like', 'videojuego', $this->videojuego])
            ->andFilterWhere(['like', 'etiqueta', $this->etiqueta]);

        return $dataProvider;
    }
}
