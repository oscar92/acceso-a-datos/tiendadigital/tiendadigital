<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dlc".
 *
 * @property int|null $codigo_videojuego
 * @property string|null $dlc
 * @property int $codigo_dlc
 * @property float|null $precio
 * @property string|null $descripcion
 *
 * @property Videojuego $codigoVideojuego
 */
class Dlc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dlc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_videojuego'], 'integer'],
            [['precio'], 'number'],
            [['dlc'], 'string', 'max' => 30],
            [['descripcion'], 'string', 'max' => 5000],
            [['codigo_videojuego'], 'exist', 'skipOnError' => true, 'targetClass' => Videojuego::className(), 'targetAttribute' => ['codigo_videojuego' => 'codigo_videojuego']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_videojuego' => 'Codigo Videojuego',
            'dlc' => 'Dlc',
            'codigo_dlc' => 'Codigo Dlc',
            'precio' => 'Precio',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[CodigoVideojuego]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoVideojuego()
    {
        return $this->hasOne(Videojuego::className(), ['codigo_videojuego' => 'codigo_videojuego']);
    }
}
