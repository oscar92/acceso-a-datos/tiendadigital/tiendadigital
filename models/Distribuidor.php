<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distribuidor".
 *
 * @property int $codigo_distribuidor
 * @property string|null $nombre
 *
 * @property Videojuego[] $videojuegos
 */
class Distribuidor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'distribuidor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_distribuidor' => 'Codigo Distribuidor',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Videojuegos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVideojuegos()
    {
        return $this->hasMany(Videojuego::className(), ['codigo_distribuidor' => 'codigo_distribuidor']);
    }
}
