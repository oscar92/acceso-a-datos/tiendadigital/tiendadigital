<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Videojuego;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function action2()
    {
        return $this->render('2');
    }
    
    public function action3()
    {
        return $this->render('3');
    }
    
    public function action4()
    {
        return $this->render('4');
    }
    
    public function action5()
    {
        return $this->render('5');
    }
    
    public function action6()
    {
        return $this->render('6');
    }
    
    public function action7()
    {
        return $this->render('7');
    }
    
    public function actionCompra()
    {
        return $this->render('compra');
    }

    public function actionCcuenta()
    {
        return $this->render('ccuenta');
    }
    
    public function actionBiblioteca()
    {
        return $this->render('biblioteca');
    }
    
    public function actionBiblioteca2()
    {
        return $this->render('biblioteca2');
    }
    
    public function actionBibliotecaet()
    {
        return $this->render('bibliotecaet');
    }
    
    public function actionBibliotecaet2()
    {
        return $this->render('bibliotecaet2');
    }
    
    public function action1()
    {
        return $this->render('1');
    }
    
    public function actionCompra2()
    {
        return $this->render('compra2');
    }
    
    public function actionCompra3()
    {
        return $this->render('compra3');
    }
    
    public function actionCompra4()
    {
        return $this->render('compra4');
    }
    
    public function actionCompra5()
    {
        return $this->render('compra5');
    }
    
    public function actionCompra6()
    {
        return $this->render('compra6');
    }
    
    public function actionCompra7()
    {
        return $this->render('compra7');
    }
    
    public function actionDlc()
    {
        return $this->render('dlc');
    }
    
    public function actionCompradlc()
    {
        return $this->render('compradlc');
    }
    
    public function actionCompraedi()
    {
        return $this->render('compraedi');
    }
    

}
