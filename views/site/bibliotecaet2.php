<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\models\Videojuego;
use app\models\VideojuegoEtiqueta;
use app\models\Etiquetas;

$this->title = 'bibliotecaet2';
$this->params['breadcrumbs'][] = $this->title;
?>

<html>

	<body>
	
	<div style="border-bottom: 1px solid black; padding-left: 0px">
			
            <div style="position: relative; left: 200px; bottom: 5px;">
                        <?= Html::a('etiquetas' , ['etiquetas/index']) ?>
			</div>
            <div style="position: relative; left: 300px; bottom: 25px;">
                        <?= Html::a('organizar juegos' , ['videojuego-etiqueta/index']) ?>
			</div>
            <div style="position: relative; left: 450px; bottom: 45px;">
                        <?= Html::a('cambiar vista' , ['site/bibliotecaet']) ?>
			</div>
            <div style="position: relative; left: 550px; bottom: 64px;">
                        <?= Html::a('desaplicar etiquetas' , ['site/biblioteca2']) ?>
			</div>
		</div>
		
		<div style="display: inline-block; border-right: 1px solid black;">
			<div>
			<?= Html::a('Tienda' , ['site/index']) ?>
			</div>
			<div>
			<?= Html::a('Biblioteca' , ['site/bibliotecaet2']) ?>
			</div>
			<div>
			<p>Descargas</p>
			</div>
		</div>
            
            <div  style=" margin: 2px; position: relative; left: 100px; bottom: 130px; bottom: 50px">
                <div style="border: 1px solid black;">
                    <div style="border: 1px solid black;">
                    <?=Etiquetas::findOne(2)->etiqueta ?>
                </div>
                <div>
			<div>
                            <div style=" position: relative; top: 10px">
			<?= Html::img('@web/imagenes/mass_effect_biblioteca.JPG', ['class'=>'imgdesc']) ?>
			</div>
                            <div style=" position: relative; bottom: 60px">
                            <p class="textbib"><?= Videojuego::findOne(4)->nombre ?></p>
                            </div>
                        <div style="float: right; position: relative; bottom: 70px">
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
                        </div>
			</div>
		</div>
            
            <div>
			<div>
                            <div style=" position: relative; top: 10px">
			<?= Html::img('@web/imagenes/mass_effect_2_biblioteca.JPG', ['class'=>'imgdesc']) ?>
			</div>
                            <div style=" position: relative; bottom: 60px">
                            <p class="textbib"><?= Videojuego::findOne(5)->nombre ?></p>
                            </div>
                        <div style="float: right; position: relative; bottom: 70px">
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
                        </div>
			</div>
		</div>
            
            <div>
			<div>
                            <div style=" position: relative; top: 10px">
			<?= Html::img('@web/imagenes/mass_effect_3_biblioteca.JPG', ['class'=>'imgdesc']) ?>
			</div>
                            <div style=" position: relative; bottom: 60px">
                            <p class="textbib"><?= Videojuego::findOne(6)->nombre ?></p>
                            </div>
                        <div style="float: right; position: relative; bottom: 70px">
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
                        </div>
			</div>
		</div>
            
            <div>
			<div>
                            <div style=" position: relative; top: 10px">
			<?= Html::img('@web/imagenes/mass_effect_legendary_edition_biblioteca.JPG', ['class'=>'imgdesc']) ?>
			</div>
                            <div style=" position: relative; bottom: 60px">
                            <p class="textbib"><?= Videojuego::findOne(7)->nombre ?></p>
                            </div>
                        <div style="float: right; position: relative; bottom: 70px">
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
                        </div>
			</div>
		</div>
            </div>
            
            <div style="margin: 2px; border: 1px solid black;">
                <div style="border: 1px solid black;">
                    <?=Etiquetas::findOne(1)->etiqueta ?>
                </div>
            <div>
                    <div style=" position: relative; top: 10px">
			<?= Html::img('@web/imagenes/crusader-kings-III_biblioteca.JPG', ['class'=>'imgdesc']) ?>
			</div>
			<div style=" position: relative; bottom: 60px">
                            <p class="textbib"><?= Videojuego::findOne(1)->nombre ?></p>
                            </div>
			<div style="float: right; position: relative; bottom: 70px">
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
                        </div>
			
		</div>
            
            <div>
			<div>
                            <div style=" position: relative; top: 10px">
			<?= Html::img('@web/imagenes/europa_universalis_IV_biblioteca.JPG', ['class'=>'imgdesc']) ?>
			</div>
                            <div style=" position: relative; bottom: 60px">
                            <p class="textbib"><?= Videojuego::findOne(3)->nombre ?></p>
                            </div>
                        <div style="float: right; position: relative; bottom: 70px">
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
                        </div>
			</div>
		</div>
            </div>
		
                <div style="margin: 2px; border: 1px solid black;">
                <div style="border: 1px solid black;">
                    <p>sin categoria</p>
                </div>
            <div>
			<div>
                            <div style=" position: relative; top: 10px">
			<?= Html::img('@web/imagenes/cyberpunk_biblioteca.JPG', ['class'=>'imgdesc']) ?>
			</div>
			<div style=" position: relative; bottom: 60px">
                            <p class="textbib"><?= Videojuego::findOne(2)->nombre ?></p>
                            </div>
                        <div style="float: right; position: relative; bottom: 70px">
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
                        </div>
			</div>
		</div>
		
            </div>

	</body>

</html>