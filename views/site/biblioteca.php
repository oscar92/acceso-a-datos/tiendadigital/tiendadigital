<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\models\Videojuego;


$this->title = 'biblioteca';
$this->params['breadcrumbs'][] = $this->title;
?>

<html>

	<body>
	
	<div style="border-bottom: 1px solid black;">
            <div style="position: relative; left: 200px; top: 3px">
                        <?= Html::a('etiquetas' , ['etiquetas/index']) ?>
			</div>
            <div style="position: relative; left: 380px; bottom: 18px">
                        <?= Html::a('organizar juegos' , ['videojuego-etiqueta/index']) ?>
			</div>
            <div style="position: relative; left: 600px; bottom: 33px;">
                        <?= Html::a('cambiar vista' , ['site/biblioteca2']) ?>
			</div>
            <div style="position: relative; left: 720px; bottom: 56px;">
                        <?= Html::a('aplicar etiquetas' , ['site/bibliotecaet']) ?>
			</div>
		</div>
		
		<div style="display: inline-block; border-right: 1px solid black;">
			<div>
			<?= Html::a('Tienda' , ['site/index']) ?>
			</div>
			<div>
			<?= Html::a('Biblioteca' , ['site/biblioteca']) ?>
			</div>
			<div>
			<p>Descargas</p>
			</div>
		</div>
		
                <div style="padding-right: 1000px; position: relative; left: 100px; bottom: 55px; ">
			<div>
			<?= Html::img('@web/imagenes/crusader-kings-III_biblioteca.JPG', ['class'=>'imagen']) ?>
			</div>
			<div style="border: 1px solid black;  position: relative;">
			<p class="textbib"><?= Videojuego::findOne(1)->nombre ?></p>
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
			</div>
		</div>
            
		<div style="padding-right: 1000px; position: relative; left: 260px; bottom: 297px;">
			<div>
			<?= Html::img('@web/imagenes/cyberpunk_biblioteca.JPG', ['class'=>'imagen']) ?>
			</div>
			<div style="border: 1px solid black; position: relative; ">
			<p class="textbib"><?= Videojuego::findOne(2)->nombre ?></p>
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
			</div>
		</div>
            
            <div style="padding-right: 1000px; position: relative; left: 425px; bottom: 538px;">
			<div>
			<?= Html::img('@web/imagenes/europa_universalis_IV_biblioteca.JPG', ['class'=>'imagen']) ?>
			</div>
			<div style="border: 1px solid black; position: relative; ">
			<p class="textbib"><?= Videojuego::findOne(3)->nombre ?></p>
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
			</div>
		</div>
            
            <div style="padding-right: 1000px; position: relative; left: 620px; bottom: 780px;">
			<div>
			<?= Html::img('@web/imagenes/mass_effect_biblioteca.JPG', ['class'=>'imagen']) ?>
			</div>
			<div style="border: 1px solid black; position: relative; ">
			<p class="textbib"><?= Videojuego::findOne(4)->nombre ?></p>
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
			</div>
		</div>
            
            <div style="padding-right: 1000px; position: relative; left: 780px; bottom: 1022px;">
			<div>
			<?= Html::img('@web/imagenes/mass_effect_2_biblioteca.JPG', ['class'=>'imagen']) ?>
			</div>
			<div style="border: 1px solid black; position: relative; ">
			<p class="textbib"><?= Videojuego::findOne(5)->nombre ?></p>
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
			</div>
		</div>
            
            <div style="padding-right: 1000px; position: relative; left: 950px; bottom: 1265px;">
			<div>
			<?= Html::img('@web/imagenes/mass_effect_3_biblioteca.JPG', ['class'=>'imagen']) ?>
			</div>
			<div style="border: 1px solid black; position: relative; ">
			<p class="textbib"><?= Videojuego::findOne(6)->nombre ?></p>
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
			</div>
		</div>
            
            <div style="padding-right: 1000px; position: relative; left: 100px; bottom: 1265px;">
			<div>
			<?= Html::img('@web/imagenes/mass_effect_legendary_edition_biblioteca.JPG', ['class'=>'imagen']) ?>
			</div>
			<div style="border: 1px solid black; position: relative; ">
			<p class="textbib"><?= Videojuego::findOne(7)->nombre ?></p>
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
			</div>
		</div>
		
	</body>

</html>