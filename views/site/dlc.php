<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\models\Dlc;
use app\models\Version;
use app\models\Videojuego;

$this->title = 'dlc';
$this->params['breadcrumbs'][] = $this->title;
?>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="site.css">
    </head>
    <body>
         <?= Html::a(Html::img('@web/imagenes/boton_retroceso.png', ['class'=>'retroceso']), ['site/1' ]) ?>
        
        
        
        <div style="text-align: right">
            <tr>
                <td>
                    <p class="titulo"><u><?= Dlc::findOne(1)->dlc ?></u></p>
                </td>
                
        </div>
		
		<div style="text-align: center; position: relative; bottom: 10px">
            <?= Html::img('@web/imagenes/northen_lords.JPG') ?>
        </div>
        
        <div style="border: 1px solid black; text-align: center; margin-left: 200px; margin-right: 200px">
            <p>Requiere: <?= Videojuego::findOne(1)->nombre ?></p>
            
        </div>
		
		
		<div style="text-align: center; border: 1px solid black; margin-left: 200px;  margin-right: 200px;">
				<td style="border-bottom: 1px solid black;">
					<p style=""><?= Dlc::findOne(1)->dlc ?></p>
					
                <div style="background-color: black; color: white;">
                    <p><?= Dlc::findOne(1)->precio ?><p>
                </div>
                 <div style="background-color: greenyellow;">
                    <?php if (! Yii::$app->user->isGuest ){ ?>
                     <?= Html::a('comprar' , ['site/compradlc']) ?>
                         
                      <?php } else { ?>
                    <?= Html::a('comprar' , ['/site/login']) ?>
                     <?php } ?>
                </div>
                    </a>
            
				</td>
			</tr>
		</div>
        
        <div style="text-align: center"
            <td>
                <strong><p class="titulo"><u>Acerca del complemento</u></p></strong>
                </td>
                <td>
                    <?= Dlc::findOne(1)->descripcion ?>
                </td>
        </div>
    </body>
</html>