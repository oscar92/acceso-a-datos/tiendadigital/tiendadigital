<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\models\Videojuego;
use app\models\Distribuidor;
use app\models\Desarrollador;
use app\models\Genero;
use app\models\Version;
use app\models\Analista;
use app\models\AnalistaVideojuego;
use app\models\Comentarios;

$this->title = '7';
$this->params['breadcrumbs'][] = $this->title;

?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="site.css">
    </head>
    <body>
         <?= Html::a(Html::img('@web/imagenes/boton_retroceso.png', ['class'=>'retroceso']), ['/site/index']) ?>
        
        
        
        <div style="text-align: right">
            <tr>
                <td>
                   <strong> <?= Videojuego::findOne(7)->nombre ?> </strong>
                </td>
                <td>
                    <p>FECHA DE LANZAMIENTO: <?= Videojuego::findOne(7)->fecha_lanzamiento ?><br>
                        DESARROLLADOR: <?= Desarrollador::findOne(3)->nombre ?><br>
                        EDITOR: <?= Distribuidor::findOne(2)->nombre ?></p>
                </td>
                <td>
                    <strong><p class="titulo"><u>Requisitos</u></p></strong>
                </td>
                <td>
                    <p>SO: <?= Videojuego::findOne(7)->SO ?><br>
                    Procesador: <?= Videojuego::findOne(7)->procesador ?><br>
                    Memoria: <?= Videojuego::findOne(7)->memoria ?><br>
                    Gráficos: <?= Videojuego::findOne(7)->grafica ?><br>
                    Almacenamiento: <?= Videojuego::findOne(7)->almacenamiento ?></p>
                </td>
                <td>
                    <strong><p class="titulo"><u>Generos</u></p></strong>
                </td>
                <td>
                    <p><?= Genero::findOne(10)->genero ?>, <?= Genero::findOne(1)->genero ?>, <?= Genero::findOne(3)->genero ?></p>
                </td>
            </tr>
        </div>
		
		<div style="text-align: center; position: relative; bottom: 300px">
            <?= Html::img('@web/imagenes/mass_effect_legendary_edition_tienda.JPG') ?>
        </div>
        
        <div style="border: 1px solid black; text-align: center; margin-left: 200px; margin-right: 200px">
            <?= Version::findOne(8)->version ?>
            <td ><a href="compra">
                <div style="background-color: black; color: white;">
                    <?= Version::findOne(8)->precio ?>
                </div>
                 <div style="background-color: greenyellow;">
                     <?php if (! Yii::$app->user->isGuest ){ ?>
                     <?= Html::a('comprar' , ['site/compra7']) ?>
                         
                      <?php } else { ?>
                    <?= Html::a('comprar' , ['/site/login']) ?>
                     <?php } ?>
                </div>
                    </a>
            </td>
        </div>
        
        <div style="text-align: center"
            <td>
                <strong><p class="titulo"><u>Acerca del juego</u></p></strong>
                </td>
                <td>
                    <?= Videojuego::findOne(7)->descripcion ?>
                </td>
        </div>
        
        <div style="text-align: center;">
            <strong><p class="titulo"><u>Calificaciones</u></p></strong>
			
            
            <div style="text-align: left; margin-left: 80px; position:relative; top: 150px">
                <td>
                    <?= Analista::findOne(6)->nombre ?>
                </td>
                <br>
                <td>
                    <?= AnalistaVideojuego::findOne(13)->puntuacion ?><br>
                    <?= AnalistaVideojuego::findOne(13)->analisis ?>
                </td>
            </div>
            <div style="text-align: center; float: inside ">
                <td>
                    <?= Analista::findOne(11)->nombre ?>
                </td>
                <br>
                <td>
                    <?= AnalistaVideojuego::findOne(14)->puntuacion ?><br>
                    <?= AnalistaVideojuego::findOne(14)->analisis ?>
                </td>
            </div>
            <div style="text-align: right; float: right; margin-right: 80px; position:relative; bottom: 145px">
                <td>
                    <?= Analista::findOne(12)->nombre ?>
                </td>
                <br>
                <td>
                    <?= AnalistaVideojuego::findOne(15)->puntuacion ?><br>
                    <?= AnalistaVideojuego::findOne(15)->analisis ?>
                </td>
            </div>
           
        </div> 
        
        <div style="position: relative; top: 70px">
            <div style="border-bottom: 1px solid black; text-align: center">
                <p>Comentarios</p>
		</div>
            <br>
            <div style="text-align: center;">
            <?php if (! Yii::$app->user->isGuest ){ ?>
                     <?= Html::a('Comentar', ['comentarios/create'], ['class' => 'btn btn-success']) ?>
                         
                      <?php } else { ?>
                    <?= Html::a('Comentar' , ['/site/login'], ['class' => 'btn btn-success']) ?>
                     <?php } ?>
            </div>
            <br>
            
        </div>
        
    </body>
</html>
