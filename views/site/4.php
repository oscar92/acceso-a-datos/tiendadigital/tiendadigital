<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\models\Videojuego;
use app\models\Distribuidor;
use app\models\Desarrollador;
use app\models\Genero;
use app\models\Version;
use app\models\Analista;
use app\models\AnalistaVideojuego;
use app\models\Comentarios;

$this->title = '4';
$this->params['breadcrumbs'][] = $this->title;

?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="site.css">
    </head>
    <body>
         <?= Html::a(Html::img('@web/imagenes/boton_retroceso.png', ['class'=>'retroceso']), ['/site/index']) ?>
        
        
        
        <div style="text-align: right">
            <tr>
                <td>
                   <strong> <?= Videojuego::findOne(4)->nombre ?> </strong>
                </td>
                <td>
                    <p>FECHA DE LANZAMIENTO: <?= Videojuego::findOne(4)->fecha_lanzamiento ?><br>
                        DESARROLLADOR: <?= Desarrollador::findOne(3)->nombre ?><br>
                        EDITOR: <?= Distribuidor::findOne(2)->nombre ?></p>
                </td>
                <td>
                    <strong><p class="titulo"><u>Requisitos</u></p></strong>
                </td>
                <td>
                    <p>SO: <?= Videojuego::findOne(4)->SO ?><br>
                    Procesador: <?= Videojuego::findOne(4)->procesador ?><br>
                    Memoria: <?= Videojuego::findOne(4)->memoria ?><br>
                    Gráficos: <?= Videojuego::findOne(4)->grafica ?><br>
                    Almacenamiento: <?= Videojuego::findOne(4)->almacenamiento ?></p>
                </td>
                <td>
                    <strong><p class="titulo"><u>Generos</u></p></strong>
                </td>
                <td>
                    <p><?= Genero::findOne(2)->genero ?>, <?= Genero::findOne(10)->genero ?>, <?= Genero::findOne(1)->genero ?></p>
                </td>
            </tr>
        </div>
		
		<div style="text-align: center; position: relative; bottom: 300px">
            <?= Html::img('@web/imagenes/mass_effect_tienda.JPG') ?>
        </div>
        
        <div style="border: 1px solid black; text-align: center; margin-left: 200px; margin-right: 200px">
            <?= Version::findOne(5)->version ?>
            <td ><a href="compra">
                <div style="background-color: black; color: white;">
                    <?= Version::findOne(5)->precio ?>
                </div>
                 <div style="background-color: greenyellow;">
                     <?php if (! Yii::$app->user->isGuest ){ ?>
                     <?= Html::a('comprar' , ['site/compra4']) ?>
                         
                      <?php } else { ?>
                    <?= Html::a('comprar' , ['/site/login']) ?>
                     <?php } ?>
                </div>
                    </a>
            </td>
        </div>
        
        <div style="text-align: center"
            <td>
                <strong><p class="titulo"><u>Acerca del juego</u></p></strong>
                </td>
                <td>
                    <?= Videojuego::findOne(4)->descripcion ?>
                </td>
        </div>
        
        <div style="text-align: center;">
            <strong><p class="titulo"><u>Calificaciones</u></p></strong>
			
            
            <div style="text-align: left; margin-left: 80px; position:relative; top: 150px">
                <td>
                    <?= Analista::findOne(8)->nombre ?>
                </td>
                <br>
                <td>
                    <?= AnalistaVideojuego::findOne(10)->puntuacion ?><br>
                    <?= AnalistaVideojuego::findOne(10)->analisis ?>
                </td>
            </div>
            <div style="text-align: center; float: inside ">
                <td>
                    <?= Analista::findOne(9)->nombre ?>
                </td>
                <br>
                <td>
                    <?= AnalistaVideojuego::findOne(11)->puntuacion ?><br>
                    <?= AnalistaVideojuego::findOne(11)->analisis ?>
                </td>
            </div>
            
           
        </div> 
        
        <div style="position: relative; top: 70px">
            <div style="border-bottom: 1px solid black; text-align: center">
                <p>Comentarios</p>
		</div>
            <br>
            <div style="text-align: center;">
            <?php if (! Yii::$app->user->isGuest ){ ?>
                     <?= Html::a('Comentar', ['comentarios/create'], ['class' => 'btn btn-success']) ?>
                         
                      <?php } else { ?>
                    <?= Html::a('Comentar' , ['/site/login'], ['class' => 'btn btn-success']) ?>
                     <?php } ?>
            </div>
            <br>
            
        </div>
        
    </body>
</html>
