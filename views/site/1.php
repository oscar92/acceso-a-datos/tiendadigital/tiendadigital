<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\models\Videojuego;
use app\models\Distribuidor;
use app\models\Desarrollador;
use app\models\Genero;
use app\models\Version;
use app\models\Analista;
use app\models\AnalistaVideojuego;
use app\models\Dlc;
use app\models\Comentarios;
$this->title = '1';
$this->params['breadcrumbs'][] = $this->title;
?>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="site.css">
    </head>
    <body>
         <?= Html::a(Html::img('@web/imagenes/boton_retroceso.png', ['class'=>'retroceso']), ['/site/index']) ?>
        <div style="text-align: right">
            <tr>
                <td>
                    <strong><?= Videojuego::findOne(1)->nombre ?></strong>
                </td>
                <td>
                    <p>FECHA DE LANZAMIENTO:  <?= Videojuego::findOne(1)->fecha_lanzamiento ?><br>
                        DESARROLLADOR: <?= Desarrollador::findOne(1)->nombre ?><br>
                        EDITOR: <?= Distribuidor::findOne(1)->nombre ?></p>
                </td>
                <td>
                    <strong><p class="titulo"><u>Requisitos</u></p></strong>
                </td>
                <td>
                    <p>SO: <?= Videojuego::findOne(1)->SO ?><br>
                    Procesador: <?= Videojuego::findOne(1)->procesador ?><br>
                    Memoria: <?= Videojuego::findOne(1)->memoria ?><br>
                    Gráficos: <?= Videojuego::findOne(1)->grafica ?><br>
                    Almacenamiento: <?= Videojuego::findOne(1)->almacenamiento ?></p>
                </td>
                <td>
                    <strong><p class="titulo"><u>Generos</u></p></strong>
                </td>
                <td>
                    <p><?= Genero::findOne(3)->genero ?>, <?= Genero::findOne(4)->genero ?>, <?= Genero::findOne(5)->genero ?></p>
                </td>
            </tr>
        </div>
		
		<div style="text-align: center; position: relative; bottom: 300px">
           <?= Html::img('@web/imagenes/crusader_kings_III_tienda.JPG') ?>
        </div>
        
        <div style="border: 1px solid black; text-align: center; margin-left: 200px; margin-right: 200px">
            <?= Version::findOne(1)->version ?>
                <div style="background-color: black; color: white;">
                    <?= Version::findOne(1)->precio ?>
                </div>
                 <div style="background-color: greenyellow;">
                    <?php if (! Yii::$app->user->isGuest ){ ?>
                     <?= Html::a('comprar' , ['site/compra2']) ?>
                         
                      <?php } else { ?>
                    <?= Html::a('comprar' , ['/site/login']) ?>
                     <?php } ?>
                </div>
        </div>
		
		<div style="border: 1px solid black; text-align: center; margin-left: 200px; margin-right: 200px">
            <?= Version::findOne(2)->version ?>
            <td >
                <div style="background-color: black; color: white;">
                    <?= Version::findOne(2)->precio ?>
                </div>
                 <div style="background-color: greenyellow;">
                   <?php if (! Yii::$app->user->isGuest ){ ?>
                     <?= Html::a('comprar' , ['site/compraedi']) ?>
                         
                      <?php } else { ?>
                    <?= Html::a('comprar' , ['/site/login']) ?>
                     <?php } ?>
                </div>
                    
            </td>
        </div>
		
		<div style="text-align: center; border: 1px solid black; margin-left: 200px;  margin-right: 200px; margin-top: 5px">
			<tr>
				<td style="border-bottom: 1px solid black">
				Complementos
				</td>
		</div>
		<div style="text-align: center; border: 1px solid black; margin-left: 200px;  margin-right: 200px;">
				<td style="border-bottom: 1px solid black;">
					 <?= Html::img('@web/imagenes/northen_lords.JPG', ['class'=>'retroceso']) ?>
                                    <p style="float: left; position: relative; left: 280px; top: 10px"> <?= Dlc::findOne(1)->dlc ?></p>
					<p style="padding-top: 50px"></p>
					
                <div style="background-color: black; color: white;">
                    <?= Dlc::findOne(1)->precio ?>
                </div>
                 <div style="background-color: greenyellow;">
                    <?= Html::a('comprar' , ['/site/dlc']) ?>
                </div>
                    </a>
            
				</td>
			</tr>
		</div>
        
        <div style="text-align: center"
            <td>
                <strong><p class="titulo"><u>Acerca del juego</u></p></strong>
                </td>
                <td>
                    <?= Videojuego::findOne(1)->descripcion ?>
                </td>
        </div>
        
        <div style="text-align: center;">
            <strong><p class="titulo"><u>Calificaciones</u></p></strong>
			
            
            <div style="text-align: left; margin-left: 80px; position:relative; top: 150px">
                <td>
                    <?= Analista::findOne(5)->nombre ?>
                </td>
                <br>
                <td>
                    <?= AnalistaVideojuego::findOne(3)->puntuacion ?><br>
                    <?= AnalistaVideojuego::findOne(3)->analisis ?>
                </td>
            </div>
            <div style="text-align: center; float: inside ">
                <td>
                    <?= Analista::findOne(4)->nombre ?>
                </td>
                <br>
                <td>
                    <?= AnalistaVideojuego::findOne(2)->puntuacion ?><br>
                    <?= AnalistaVideojuego::findOne(2)->analisis ?>
                </td>
            </div>
            <div style="text-align: right; float: right; margin-right: 80px; position:relative; bottom: 150px">
                <td>
                    <?= Analista::findOne(3)->nombre ?>
                </td>
                <br>
                <td>
                    <?= AnalistaVideojuego::findOne(1)->puntuacion ?><br>
                    <?= AnalistaVideojuego::findOne(1)->analisis ?>
                </td>
            </div>
           
        </div>
        
        <div style="position: relative; top: 70px">
            <div style="border-bottom: 1px solid black; text-align: center">
                <p>Comentarios</p>
		</div>
            <br>
            <div style="text-align: center;">
                <?php if (! Yii::$app->user->isGuest ){ ?>
                     <?= Html::a('Comentar', ['comentarios/create'], ['class' => 'btn btn-success']) ?>
                         
                      <?php } else { ?>
                    <?= Html::a('Comentar' , ['/site/login'], ['class' => 'btn btn-success']) ?>
                     <?php } ?>
            </div>
            <br>
            <div style=" text-align: center; border: 1px solid black; margin-left: 200px; margin-right: 400px">
                <?= Comentarios::findOne(1)->usuario ?> <div style="position: relative; bottom: 15px"><?= Html::img('@web/imagenes/dislike.png', ['class'=>'enlaces']) ?></div> <br>
                    <?= Comentarios::findOne(1)->comentario ?>
		</div>
        </div>
        
    </body>
</html>