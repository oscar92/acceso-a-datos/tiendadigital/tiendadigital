<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\models\Videojuego;


$this->title = 'biblioteca2';
$this->params['breadcrumbs'][] = $this->title;
?>

<html>

	<body>
	
	<div style="border-bottom: 1px solid black;">
			
            <div style="position: relative; left: 200px; bottom: 5px;">
                        <?= Html::a('etiquetas' , ['etiquetas/index']) ?>
			</div>
            <div style="position: relative; left: 300px; bottom: 25px;">
                        <?= Html::a('organizar juegos' , ['videojuego-etiqueta/index']) ?>
			</div>
            <div style="position: relative; left: 450px; bottom: 45px;">
                        <?= Html::a('cambiar vista' , ['site/biblioteca']) ?>
			</div>
            
            <div style="position: relative; left: 550px; bottom: 64px;">
                        <?= Html::a('aplicar etiquetas' , ['site/bibliotecaet2']) ?>
			</div>
		</div>
		
		<div style="display: inline-block; border-right: 1px solid black;">
			<div>
			<?= Html::a('Tienda' , ['site/index']) ?>
			</div>
			<div>
			<?= Html::a('Biblioteca' , ['site/biblioteca2']) ?>
			</div>
			<div>
			<p>Descargas</p>
			</div>
		</div>
            
            <div style="position: relative; left: 100px; bottom: 130px;">
                    <div style="position: relative; position: relative; top: 75px">
			<?= Html::img('@web/imagenes/crusader-kings-III_biblioteca.JPG', ['class'=>'imgdesc']) ?>
			</div>
			<div>
			<p class="textbib"><?= Videojuego::findOne(1)->nombre ?></p>
			<div style="float: right; position: relative; bottom: 10px">
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
                        </div>
			</div>
		</div>
		
		<div style=" position: relative; left: 100px; bottom: 130px;">
			<div>
                            <div style="position: relative; position: relative; top: 75px">
			<?= Html::img('@web/imagenes/cyberpunk_biblioteca.JPG', ['class'=>'imgdesc']) ?>
			</div>
			<p class="textbib"><?= Videojuego::findOne(2)->nombre ?></p>
                        <div style="float: right; position: relative; bottom: 10px">
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
                        </div>
			</div>
		</div>
            
            <div style=" position: relative; left: 100px; bottom: 130px;">
			<div>
                            <div style="position: relative; position: relative; top: 75px">
			<?= Html::img('@web/imagenes/europa_universalis_IV_biblioteca.JPG', ['class'=>'imgdesc']) ?>
			</div>
                            <p class="textbib"><?= Videojuego::findOne(3)->nombre ?></p>
                        <div style="float: right; position: relative; bottom: 10px">
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
                        </div>
			</div>
		</div>
            
            <div style=" position: relative; left: 100px; bottom: 130px;">
			<div>
                            <div style="position: relative; position: relative; top: 75px">
			<?= Html::img('@web/imagenes/mass_effect_biblioteca.JPG', ['class'=>'imgdesc']) ?>
			</div>
                            <p class="textbib"><?= Videojuego::findOne(4)->nombre ?></p>
                        <div style="float: right; position: relative; bottom: 10px">
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
                        </div>
			</div>
		</div>
            
            <div style=" position: relative; left: 100px; bottom: 130px;">
			<div>
                            <div style="position: relative; position: relative; top: 75px">
			<?= Html::img('@web/imagenes/mass_effect_2_biblioteca.JPG', ['class'=>'imgdesc']) ?>
			</div>
                            <p class="textbib"><?= Videojuego::findOne(5)->nombre ?></p>
                        <div style="float: right; position: relative; bottom: 10px">
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
                        </div>
			</div>
		</div>
            
            <div style=" position: relative; left: 100px; bottom: 130px;">
			<div>
                            <div style="position: relative; position: relative; top: 75px">
			<?= Html::img('@web/imagenes/mass_effect_3_biblioteca.JPG', ['class'=>'imgdesc']) ?>
			</div>
                            <p class="textbib"><?= Videojuego::findOne(6)->nombre ?></p>
                        <div style="float: right; position: relative; bottom: 10px">
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
                        </div>
			</div>
		</div>
            
            <div style=" position: relative; left: 100px; bottom: 130px;">
			<div>
                            <div style="position: relative; position: relative; top: 75px">
			<?= Html::img('@web/imagenes/mass_effect_legendary_edition_biblioteca.JPG', ['class'=>'imgdesc']) ?>
			</div>
                            <p class="textbib"><?= Videojuego::findOne(7)->nombre ?></p>
                        <div style="float: right; position: relative; bottom: 10px">
			<?= Html::img('@web/imagenes/descarga.PNG', ['class'=>'imgdesc']) ?>
                        </div>
			</div>
		</div>

	</body>

</html>