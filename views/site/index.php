<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use app\models\Version;
$this->title = 'index';
$this->params['breadcrumbs'][] = $this->title;
?>
<html>
        <?= Html::a(Html::img('@web/imagenes/icono_usuario.png', ['class'=>'enlaces']), ['/site/login']) ?>
    <div style="float: right; margin-right: 50px;">
    <?= Html::a('buscador' , ['videojuego/index'], ['class' => 'btn btn-success']) ?>
    </div>
    <?php if (! Yii::$app->user->isGuest ){?>
    <div style="display: inline-block; border-right: 1px solid black;">
			<div>
			<?= Html::a('Tienda' , ['site/index']) ?>
			</div>
			<div>
			<?= Html::a('Biblioteca' , ['site/biblioteca']) ?>
			</div>
			<div>
			<p>Descargas</p>
			</div>
		</div>
    <?php }?>
    
    <div>
        <b><u><H3 style="text-align: center; ">novedades</H3></u></b>
        <div class="jumbotron">
       <?= Html::a(Html::img('@web/imagenes/cyberpunk_tienda.JPG') , ['site/2' ]) ?>
            <div style="border: 1px solid black; margin-left: 280px; margin-right: 280px;">
                <?= Version::findOne(3)->precio ?> 
            </div>
    </div>
    </div>
    
    <div>
        <b><u><H4 style="text-align: center; ">juegos de Paradox</H4></u></b>
        <div style="position: relative; left: 125px">
            <div>
                <?= Html::a(Html::img('@web/imagenes/crusader_kings_III_tienda.JPG', ['class'=>'imagen']) , ['site/1' ]) ?>
                <div style=" border: 1px solid black; margin-right: 1005px; margin-left: 5px">
                    <?= Version::findOne(1)->precio ?>
                </div>
            </div>
    
            <div style="position: relative; left: 160px; bottom: 182px">
                <?= Html::a(Html::img('@web/imagenes/europa_universalis_IV_tienda.JPG', ['class'=>'imagen']) , ['site/3' ]) ?>
                <div style=" border: 1px solid black; margin-right: 1005px; margin-left: 5px">
                    <?= Version::findOne(4)->precio ?>
                </div>
            </div>
        </div>
    </div>
    
    <div>
        <b><u><H4 style="text-align: center; ">franquicia Mass Effect</H4></u></b>
        <div style="position: relative; left: 125px">
            <div>
                <?= Html::a(Html::img('@web/imagenes/mass_effect_tienda.JPG', ['class'=>'imagen']) , ['site/4' ]) ?>
                <div style=" border: 1px solid black; margin-right: 1005px; margin-left: 5px">
                    <?= Version::findOne(5)->precio ?>
                </div>
            </div>
    
            <div style="position: relative; left: 160px; bottom: 182px">
                <?= Html::a(Html::img('@web/imagenes/mass_effect_2_tienda.JPG', ['class'=>'imagen']) , ['site/5' ]) ?>
                <div style=" border: 1px solid black; margin-right: 1005px; margin-left: 5px">
                    <?= Version::findOne(6)->precio ?>
                </div>
            </div>
    
            <div style="position: relative; left: 320px; bottom: 364px">
                <?= Html::a(Html::img('@web/imagenes/mass_effect_3_tienda.JPG', ['class'=>'imagen']) , ['site/6' ]) ?>
                <div style=" border: 1px solid black; margin-right: 1005px; margin-left: 5px">
                    <?= Version::findOne(7)->precio ?>
                </div>
            </div>
    
            <div style="position: relative; left: 480px; bottom: 546px">
                <?= Html::a(Html::img('@web/imagenes/mass_effect_legendary_edition_tienda.JPG', ['class'=>'imagen']) , ['site/7' ]) ?>
                <div style=" border: 1px solid black; margin-right: 1005px; margin-left: 5px">
                    <?= Version::findOne(7)->precio ?>
                </div>
            </div>
        </div>
    </div>
</html>