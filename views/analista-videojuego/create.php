<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AnalistaVideojuego */

$this->title = 'Create Analista Videojuego';
$this->params['breadcrumbs'][] = ['label' => 'Analista Videojuegos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="analista-videojuego-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
