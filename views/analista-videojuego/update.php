<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AnalistaVideojuego */

$this->title = 'Update Analista Videojuego: ' . $model->codigo_analista_videojuego;
$this->params['breadcrumbs'][] = ['label' => 'Analista Videojuegos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_analista_videojuego, 'url' => ['view', 'id' => $model->codigo_analista_videojuego]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="analista-videojuego-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
