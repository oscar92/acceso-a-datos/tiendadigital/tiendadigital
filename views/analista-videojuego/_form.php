<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AnalistaVideojuego */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="analista-videojuego-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_analista')->textInput() ?>

    <?= $form->field($model, 'codigo_videojuego')->textInput() ?>

    <?= $form->field($model, 'puntuacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'analisis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_analista_videojuego')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
