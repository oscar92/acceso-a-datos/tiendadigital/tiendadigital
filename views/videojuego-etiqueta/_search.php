<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VideojuegoEtiquetaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="videojuego-etiqueta-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'videojuego') ?>

    <?= $form->field($model, 'etiqueta') ?>

    <?= $form->field($model, 'codigo_videojuego_etiqueta') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
