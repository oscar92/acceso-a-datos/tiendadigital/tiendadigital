<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VideojuegoEtiqueta */

$this->title = 'Create Videojuego Etiqueta';
$this->params['breadcrumbs'][] = ['label' => 'Videojuego Etiquetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="videojuego-etiqueta-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
