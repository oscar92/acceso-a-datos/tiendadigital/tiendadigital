<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VideojuegoEtiqueta */

$this->title = 'Update Videojuego Etiqueta: ' . $model->codigo_videojuego_etiqueta;
$this->params['breadcrumbs'][] = ['label' => 'Videojuego Etiquetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_videojuego_etiqueta, 'url' => ['view', 'id' => $model->codigo_videojuego_etiqueta]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="videojuego-etiqueta-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
