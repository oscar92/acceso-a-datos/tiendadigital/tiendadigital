<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VideojuegoEtiquetaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Videojuego Etiquetas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="videojuego-etiqueta-index">
<?= Html::a(Html::img('@web/imagenes/boton_retroceso.png', ['class'=>'retroceso']), ['site/biblioteca' ]) ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Videojuego Etiqueta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'videojuego',
            'etiqueta',
            'codigo_videojuego_etiqueta',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
