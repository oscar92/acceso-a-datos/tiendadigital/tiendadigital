<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VideojuegoEtiqueta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="videojuego-etiqueta-form">
<?= Html::a(Html::img('@web/imagenes/boton_retroceso.png', ['class'=>'retroceso']), ['videojuego-etiqueta/index' ]) ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'videojuego')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'etiqueta')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
