<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Analista */

$this->title = 'Update Analista: ' . $model->codigo_analista;
$this->params['breadcrumbs'][] = ['label' => 'Analistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_analista, 'url' => ['view', 'id' => $model->codigo_analista]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="analista-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
