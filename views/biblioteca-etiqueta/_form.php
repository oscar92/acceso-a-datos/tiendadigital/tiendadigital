<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BibliotecaEtiqueta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="biblioteca-etiqueta-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_biblioteca')->textInput() ?>

    <?= $form->field($model, 'codigo_etiqueta')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
