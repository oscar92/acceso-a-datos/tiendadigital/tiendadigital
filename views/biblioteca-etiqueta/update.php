<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BibliotecaEtiqueta */

$this->title = 'Update Biblioteca Etiqueta: ' . $model->codigo_biblioteca_etiqueta;
$this->params['breadcrumbs'][] = ['label' => 'Biblioteca Etiquetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_biblioteca_etiqueta, 'url' => ['view', 'id' => $model->codigo_biblioteca_etiqueta]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="biblioteca-etiqueta-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
