<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BibliotecaEtiqueta */

$this->title = 'Create Biblioteca Etiqueta';
$this->params['breadcrumbs'][] = ['label' => 'Biblioteca Etiquetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="biblioteca-etiqueta-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
