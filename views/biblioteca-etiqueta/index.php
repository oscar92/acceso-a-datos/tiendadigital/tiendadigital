<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Biblioteca Etiquetas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="biblioteca-etiqueta-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Biblioteca Etiqueta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_biblioteca',
            'codigo_etiqueta',
            'codigo_biblioteca_etiqueta',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
