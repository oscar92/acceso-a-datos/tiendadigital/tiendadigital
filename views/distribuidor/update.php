<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuidor */

$this->title = 'Update Distribuidor: ' . $model->codigo_distribuidor;
$this->params['breadcrumbs'][] = ['label' => 'Distribuidors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_distribuidor, 'url' => ['view', 'id' => $model->codigo_distribuidor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="distribuidor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
