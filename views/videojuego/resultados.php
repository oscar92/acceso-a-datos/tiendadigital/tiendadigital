<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Videojuegos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="videojuego-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Videojuego', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_videojuego',
            'codigo_desarrollador',
            'codigo_distribuidor',
            'nombre',
            'fecha_lanzamiento',
            //'descrpicion',
            //'SO',
            //'procesador',
            //'memoria',
            //'grafica',
            //'almacenamiento',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

