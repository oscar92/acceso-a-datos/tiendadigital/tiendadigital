<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VideojuegoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Videojuegos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="videojuego-index">
<?= Html::a(Html::img('@web/imagenes/boton_retroceso.png', ['class'=>'retroceso']), ['/site/index']) ?>
    <h1><?= Html::encode($this->title) ?></h1>

    

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'codigo_videojuego',
            //'codigo_desarrollador',
            //'codigo_distribuidor',
            'nombre',
            'fecha_lanzamiento',
            //'descripcion',
            //'SO',
            //'procesador',
            //'memoria',
            //'grafica',
            //'almacenamiento',
            

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
