<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VideojuegoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="videojuego-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigo_videojuego') ?>

    <?= $form->field($model, 'codigo_desarrollador') ?>

    <?= $form->field($model, 'codigo_distribuidor') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'fecha_lanzamiento') ?>

    <?php // echo $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'SO') ?>

    <?php // echo $form->field($model, 'procesador') ?>

    <?php // echo $form->field($model, 'memoria') ?>

    <?php // echo $form->field($model, 'grafica') ?>

    <?php // echo $form->field($model, 'almacenamiento') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
