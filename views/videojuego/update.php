<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Videojuego */

$this->title = 'Update Videojuego: ' . $model->codigo_videojuego;
$this->params['breadcrumbs'][] = ['label' => 'Videojuegos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_videojuego, 'url' => ['view', 'id' => $model->codigo_videojuego]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="videojuego-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
