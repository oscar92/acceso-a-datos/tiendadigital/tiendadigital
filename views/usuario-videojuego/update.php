<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UsuarioVideojuego */

$this->title = 'Update Usuario Videojuego: ' . $model->codigo_usuario_videojuego;
$this->params['breadcrumbs'][] = ['label' => 'Usuario Videojuegos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_usuario_videojuego, 'url' => ['view', 'id' => $model->codigo_usuario_videojuego]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usuario-videojuego-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
