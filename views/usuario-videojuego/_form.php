<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsuarioVideojuego */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-videojuego-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_usuario')->textInput() ?>

    <?= $form->field($model, 'codigo_videojuego')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
