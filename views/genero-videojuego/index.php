<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Genero Videojuegos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="genero-videojuego-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Genero Videojuego', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_genero',
            'codigo_videojuego',
            'codigo_genero_videojuego',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
