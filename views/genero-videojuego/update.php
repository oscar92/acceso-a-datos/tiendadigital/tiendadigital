<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GeneroVideojuego */

$this->title = 'Update Genero Videojuego: ' . $model->codigo_genero_videojuego;
$this->params['breadcrumbs'][] = ['label' => 'Genero Videojuegos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_genero_videojuego, 'url' => ['view', 'id' => $model->codigo_genero_videojuego]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="genero-videojuego-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
