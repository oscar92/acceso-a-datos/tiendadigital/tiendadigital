<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GeneroVideojuego */

$this->title = 'Create Genero Videojuego';
$this->params['breadcrumbs'][] = ['label' => 'Genero Videojuegos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="genero-videojuego-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
