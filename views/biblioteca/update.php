<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Biblioteca */

$this->title = 'Update Biblioteca: ' . $model->codigo_biblioteca;
$this->params['breadcrumbs'][] = ['label' => 'Bibliotecas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_biblioteca, 'url' => ['view', 'id' => $model->codigo_biblioteca]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="biblioteca-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
