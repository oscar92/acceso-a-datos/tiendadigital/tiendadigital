<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BibliotecaVideojuego */

$this->title = 'Update Biblioteca Videojuego: ' . $model->codigo_biblioteca_videojuego;
$this->params['breadcrumbs'][] = ['label' => 'Biblioteca Videojuegos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_biblioteca_videojuego, 'url' => ['view', 'id' => $model->codigo_biblioteca_videojuego]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="biblioteca-videojuego-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
