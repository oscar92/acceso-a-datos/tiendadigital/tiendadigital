<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BibliotecaVideojuego */

$this->title = 'Create Biblioteca Videojuego';
$this->params['breadcrumbs'][] = ['label' => 'Biblioteca Videojuegos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="biblioteca-videojuego-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
